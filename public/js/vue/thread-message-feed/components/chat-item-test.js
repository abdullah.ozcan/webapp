const mount = require('../../__test__/vuex-mount');
const momentTimezone = require('moment-timezone');
const {
  createSerializedMessageFixture,
  createSerializedRoomFixture
} = require('../../__test__/fixture-helpers');
const { default: ChatItem } = require('./chat-item.vue');

jest.mock('../../../components/unread-items-client');
const unreadItemsClient = require('../../../components/unread-items-client');

describe('thread-message-feed chat-item', () => {
  momentTimezone.tz.setDefault('Europe/London');
  const message = createSerializedMessageFixture();
  const defaultProps = {
    message,
    useCompactStyles: false
  };
  const addRoomToStore = store => {
    const room = createSerializedRoomFixture('abc/def');
    store.state.roomMap = { [room.id]: room };
    store.state.displayedRoomId = room.id;
  };

  describe('snapshot', () => {
    it('with default props', () => {
      const { wrapper } = mount(ChatItem, defaultProps, addRoomToStore);
      expect(wrapper.element).toMatchSnapshot();
    });
    describe('component flags', () => {
      ['useCompactStyles', 'showItemActions'].forEach(flag => {
        it(flag, () => {
          const { wrapper } = mount(
            ChatItem,
            {
              ...defaultProps,
              [flag]: true
            },
            addRoomToStore
          );
          expect(wrapper.element).toMatchSnapshot();
        });
      });
    });
    describe('message flags', () => {
      ['error', 'loading', 'unread'].forEach(flag => {
        it(flag, () => {
          const { wrapper } = mount(
            ChatItem,
            {
              ...defaultProps,
              message: { ...message, [flag]: true }
            },
            addRoomToStore
          );
          expect(wrapper.element).toMatchSnapshot();
        });
      });
    });
    it('highlighted - scrolls into view', () => {
      const scrollIntoViewMock = jest.fn();
      const { wrapper } = mount(
        ChatItem,
        {
          ...defaultProps,
          message: { ...message, highlighted: true }
        },
        addRoomToStore,
        { methods: { scrollIntoView: scrollIntoViewMock } }
      );
      expect(wrapper.element).toMatchSnapshot();
      expect(scrollIntoViewMock.mock.calls[0]).toEqual(['smooth', 'center']);
    });
  });
  it('focusedAt - scrolls into view', () => {
    const scrollIntoViewMock = jest.fn();
    mount(
      ChatItem,
      {
        ...defaultProps,
        message: { ...message, focusedAt: { block: 'start', timestamp: Date.now() } }
      },
      addRoomToStore,
      { methods: { scrollIntoView: scrollIntoViewMock } }
    );
    expect(scrollIntoViewMock.mock.calls[0]).toEqual(['auto', 'start']);
  });
  it('unread message reports itself as read when entering viewport', () => {
    const { wrapper } = mount(
      ChatItem,
      {
        ...defaultProps,
        message: { ...message, unread: true }
      },
      addRoomToStore
    );
    wrapper.vm.onViewportEnter();
    expect(unreadItemsClient.markItemRead.mock.calls[0]).toEqual([message.id]);
  });
});
